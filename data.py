import os
import torch
import pickle
import pandas as pd
import numpy as np

from torch.utils.data import Dataset

import tqdm

def createDataCSV(dataset = 'eurlex4k'):
    labels = []
    texts = []
    dataType = []
    label_map = {}

    name_map = {'eurlex4k': 'EUR-Lex'}

    assert dataset in name_map
    dataset = name_map[dataset]

    fext = '_texts.txt'
    with open(f'./{dataset}/train{fext}') as f:
        for i in tqdm.tqdm(f):
            texts.append(i.replace('\n', ''))
            dataType.append('train')

    with open(f'./{dataset}/test{fext}') as f:
        for i in tqdm.tqdm(f):
            texts.append(i.replace('\n', ''))
            dataType.append('test')

    with open(f'./{dataset}/train_labels.txt') as f:
        for i in tqdm.tqdm(f):
            for l in i.replace('\n', '').split():
                label_map[l] = 0
            labels.append(i.replace('\n', ''))


    with open(f'./{dataset}/test_labels.txt') as f:
        print(len(label_map))
        for i in tqdm.tqdm(f):
            for l in i.replace('\n', '').split():
                label_map[l] = 0
            labels.append(i.replace('\n', ''))
        print(len(label_map))

    assert len(texts) == len(labels) == len(dataType)

    df_row = {'text': texts, 'label': labels, 'dataType': dataType}

    for i, k in enumerate(sorted(label_map.keys())):
        label_map[k] = i
    df = pd.DataFrame(df_row)

    print('label map', len(label_map))

    return df, label_map